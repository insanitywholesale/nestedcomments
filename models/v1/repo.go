package models

import (
	pb "gitlab.com/insanitywholesale/nestedcomments/proto/v1"
)

type CommentRepo interface {
	RetrieveTopLevel() (*pb.Comments, error)
	RetrieveAll() (*pb.Comments, error)
	//RetrieveReplies(parentid uint32) (*pb.Comments, error)
	Save(*pb.Comment) error
	Change(*pb.Comment) error
	Remove(*pb.Comment) error
}
