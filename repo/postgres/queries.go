package postgres

var createCommentsTableQuery = `
CREATE TABLE IF NOT EXISTS Comments (
	Id SERIAL PRIMARY KEY,
	Author VARCHAR,
	Content VARCHAR,
	Created_At TIMESTAMP,
	Updated_At TIMESTAMP,
	Parent_Id SMALLINT
);`

var getTopLevelCommentsQuery = `SELECT * FROM Comments WHERE parent_id IS NULL;`

var getRepliesToCommentQuery = `SELECT * FROM Comments WHERE parent_id = $1;`

var getAllCommentsQuery = `SELECT
	Id,
	Author,
	Content,
	Created_At,
	Updated_At,
	Parent_Id
FROM Comments;`

// Source: https://www.aleksandra.codes/comments-db-model

// fix query
// ERROR:  column reference "parent_id" is ambiguous at character 221
/*
var getAllCommentsQuery = `
WITH cte_replies AS (
	SELECT
		Parent_Id,
		COALESCE(json_agg(row_to_json(replies)),
			'[]'::JSON) AS replies
	FROM
		Comments AS replies
	GROUP BY
		Parent_Id
)
SELECT
	Author,
	Content,
	Created_At,
	Updated_At,
	Parent_Id
FROM Comments c
	LEFT JOIN cte_replies ON c.Id = cte_replies.Parent_Id
WHERE
	c.Parent_Id IS NULL;`
*/

var addCommentQuery = `INSERT INTO Comments (
	Author,
	Content,
	Created_At,
	Updated_At,
	Parent_Id
) VALUES ($1, $2, $3, $4, $5);`

// RETURNING Id;`

var changeCommentQuery = `UPDATE Comments SET Content = $1 WHERE Id = $2;`

var deleteCommentQuery = `DELETE FROM Comments WHERE Id = ?;`
