package postgres

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	pb "gitlab.com/insanitywholesale/nestedcomments/proto/v1"
	tpb "google.golang.org/protobuf/types/known/timestamppb"
	"time"
)

type pgRepo struct {
	client *pgx.Conn
	pgURL  string
}

var ctx = context.Background()

func newPostgresClient(url string) (*pgx.Conn, error) {
	client, err := pgx.Connect(ctx, url)
	if err != nil {
		return nil, err
	}
	err = client.Ping(ctx)
	if err != nil {
		return nil, err
	}
	_, err = client.Exec(ctx, createCommentsTableQuery)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func NewPostgresRepo(url string) (*pgRepo, error) {
	pgclient, err := newPostgresClient(url)
	if err != nil {
		return nil, err
	}
	repo := &pgRepo{
		pgURL:  url,
		client: pgclient,
	}
	return repo, nil
}

func (r *pgRepo) RetrieveTopLevel() (*pb.Comments, error) {
	var commentSlice = []*pb.Comment{}
	err := pgxscan.Select(ctx, r.client, &commentSlice, getTopLevelCommentsQuery)
	if err != nil {
		return nil, err
	}
	return &pb.Comments{Comments: commentSlice}, nil
}

func (r *pgRepo) RetrieveAll() (*pb.Comments, error) {
	var commentSlice = []*pb.Comment{}
	rows, err := r.client.Query(ctx, getAllCommentsQuery)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var c = &pb.Comment{}
		var tc time.Time
		var tu time.Time
		err := rows.Scan(
			&c.Id,
			&c.Author,
			&c.Content,
			&tc,
			&tu,
			&c.ParentId,
		)
		c.CreatedAt = tpb.New(tc)
		c.UpdatedAt = tpb.New(tu)
		if err != nil {
			return nil, err
		}
		commentSlice = append(commentSlice, c)
	}
	rows.Close()
	return &pb.Comments{Comments: commentSlice}, nil
}

func (r *pgRepo) Save(c *pb.Comment) error {
	timeNow := tpb.Now().AsTime()
	rows, err := r.client.Query(ctx, addCommentQuery,
		c.Author,
		c.Content,
		timeNow,
		timeNow,
		c.ParentId,
	)
	rows.Close()
	if err != nil {
		return err
	}
	return nil
}

func (r *pgRepo) Change(c *pb.Comment) error {
	rows, err := r.client.Query(ctx, changeCommentQuery, c.Content, c.Id)
	rows.Close()
	if err != nil {
		return err
	}
	return nil
}

func (r *pgRepo) Remove(c *pb.Comment) error {
	_, err := r.client.Query(ctx, deleteCommentQuery, c.Id)
	if err != nil {
		return err
	}
	return nil
}
