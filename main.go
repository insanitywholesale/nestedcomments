package main

import (
	apiv1 "gitlab.com/insanitywholesale/nestedcomments/grpc/v1"
	pbv1 "gitlab.com/insanitywholesale/nestedcomments/proto/v1"
	"gitlab.com/insanitywholesale/nestedcomments/rest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"os"
)

var (
	commitHash string
	commitDate string

	grpcport string
	restport string

	serviceName string = "nestedcomments"
)

func getServiceName() string {
	return serviceName
}

func setupPorts() {
	grpcport = os.Getenv("COMMENTS_GRPC_PORT")
	if grpcport == "" {
		grpcport = "15200"
	}
	restport = os.Getenv("COMMENTS_REST_PORT")
	if restport == "" {
		restport = "9392"
	}
}

func startGRPC() {
	listener, err := net.Listen("tcp", ":"+grpcport)
	if err != nil {
		log.Fatalf("listen failed %v", err)
	}

	grpcServer := grpc.NewServer()
	pbv1.RegisterNestedCommentsServer(grpcServer, apiv1.Server{})
	reflection.Register(grpcServer)
	log.Println("grpc started on port", grpcport)
	log.Fatal(grpcServer.Serve(listener))
}

func startHTTP() {
	//rest.SaveVars(openapiDocs, commitHash, commitDate)

	log.Println("rest starting on port", restport)
	log.Fatal(rest.RunGateway(grpcport, restport))
}

func main() {
	setupPorts()
	go startGRPC()
	defer startHTTP()
}
