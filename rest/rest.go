package rest

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	gw "gitlab.com/insanitywholesale/nestedcomments/proto/v1"
	"google.golang.org/grpc"
	"net/http"
	"strings"
)

func fallback(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("idk about that fam\n"))
	return
}

func pong(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("pong\n"))
	return
}

func RunGateway(grpcport string, restport string) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	gwmux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := gw.RegisterNestedCommentsHandlerFromEndpoint(ctx, gwmux, ":"+grpcport, opts)
	if err != nil {
		return err
	}

	handler := cors.Default().Handler(gwmux)

	gwServer := &http.Server{
		Addr: ":" + restport,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasPrefix(r.URL.Path, "/api") {
				handler.ServeHTTP(w, r)
				return
			}
			if strings.HasPrefix(r.URL.Path, "/ping") {
				pong(w, r)
				return
			}
			fallback(w, r)
			return
		}),
	}
	return gwServer.ListenAndServe()
}
