package grpc

import (
	"context"
	models "gitlab.com/insanitywholesale/nestedcomments/models/v1"
	pb "gitlab.com/insanitywholesale/nestedcomments/proto/v1"
	"gitlab.com/insanitywholesale/nestedcomments/repo/postgres"
	"log"
	"os"
)

type Server struct {
	pb.UnimplementedNestedCommentsServer
}

var dbstore models.CommentRepo

func init() {
	pgURL := os.Getenv("PG_URL")
	if pgURL != "" {
		if pgURL == "test" {
			db, err := postgres.NewPostgresRepo("postgresql://tester:Apasswd@localhost:5432?sslmode=disable")
			if err != nil {
				log.Fatalf("error %v", err)
			}
			dbstore = db
		} else {
			db, err := postgres.NewPostgresRepo(pgURL)
			if err != nil {
				log.Fatalf("error %v", err)
			}
			dbstore = db
		}
		return
	} else {
		log.Fatal("environment variable PG_URL not set")
		return
	}
}

func (Server) GetTopLevelComments(context.Context, *pb.Empty) (*pb.Comments, error) {
	return dbstore.RetrieveTopLevel()
}

func (Server) GetAllComments(context.Context, *pb.Empty) (*pb.Comments, error) {
	return dbstore.RetrieveAll()
}

func (Server) AddComment(_ context.Context, c *pb.Comment) (*pb.Empty, error) {
	return &pb.Empty{}, dbstore.Save(c)
}

func (Server) UpdateComment(context.Context, *pb.Comment) (*pb.Empty, error) {
	return nil, nil
}

func (Server) DeleteComment(context.Context, *pb.Comment) (*pb.Empty, error) {
	return nil, nil
}

func (Server) GetReplies(context.Context, *pb.Comment) (*pb.Comments, error) {
	return nil, nil
}
